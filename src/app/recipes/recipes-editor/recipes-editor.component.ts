import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Recipe } from 'src/app/shared/model/recipe.model';
import { RecipesService } from '../recipes.service';


@Component({
  selector: 'app-recipes-editor',
  templateUrl: './recipes-editor.component.html',
  styleUrls: ['./recipes-editor.component.css']
})
export class RecipesEditorComponent implements OnInit {

  id: string;
  editMode = false;
  recipe: Recipe;

  title: string;

  isLinear = true;
  recipeDetailForm: FormGroup;
  recipeIngredientsForm: FormGroup;
  recipeStepsForm: FormGroup;

  constructor(
    private recipeService: RecipesService,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.editMode = params['id'] != null;
  
      this.initForm();
    });

    this.recipeDetailForm = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      imageUrl: new FormControl('', Validators.required),
      cookTime: new FormControl('', Validators.required),
      readyTime: new FormControl('', Validators.required),
      serving: new FormControl('', Validators.required),
      cuisine: new FormControl('', Validators.required)
    })

    this.recipeIngredientsForm = new FormGroup({
      ingredients: new FormArray([])
    })

    this.recipeStepsForm = new FormGroup({
      steps: new FormArray([])
    })
  }

  ngOnInit() {
    this.route.url.subscribe(data => {
      const routeData = data[data.length - 1].path;

      this.title = routeData === 'edit' ? 'Edit Recipe' : 'New Recipe'
    })
  }

  initForm() {
    let id;
    let title = '';
    let description = '';
    let imageUrl = '';
    let cookTime = '';
    let readyTime = '';
    let serving = '';
    let cuisine = '';
    let ingredients = new FormArray([]);
    let steps = new FormArray([]);

    if (this.editMode) {
      this.recipeService.getRecipeById(this.id).subscribe(
        data => {
          // console.log(data)
          const recipe = data;
          title = recipe.title;
          description = recipe.description;
          imageUrl = recipe.imageUrl;
          cookTime = recipe.cookTime;
          readyTime = recipe.readyTime;
          serving = recipe.serving;
          cuisine = recipe.cuisine;

          if (recipe.ingredients) {
            recipe.ingredients.forEach(ing => {
              const control = new FormGroup({
                ingredient: new FormControl(ing.ingredient, Validators.required),
              })
              ingredients.push(control);
            })
          }

          if (recipe.steps) {
            recipe.steps.forEach(step => {
              const control = new FormGroup({
                step: new FormControl(step.step, Validators.required),
              })
              steps.push(control);
            })
          }

          this.recipeDetailForm = new FormGroup({
            title: new FormControl(title, Validators.required),
            description: new FormControl(description, Validators.required),
            imageUrl: new FormControl(imageUrl, Validators.required),
            cookTime: new FormControl(cookTime, Validators.required),
            readyTime: new FormControl(readyTime, Validators.required),
            serving: new FormControl(serving, Validators.required),
            cuisine: new FormControl(cuisine, Validators.required)
          })

          this.recipeIngredientsForm = new FormGroup({
            ingredients: ingredients
          })

          this.recipeStepsForm = new FormGroup({
            steps: steps
          })
        },
        error => {
          console.log(error)
        }
      )
    }
  }

  get ingredientsControl() {
    return (this.recipeIngredientsForm.get('ingredients') as FormArray).controls;
  }

  onAddIngredient() {
    const ing = new FormGroup({
      ingredient: new FormControl('', Validators.required)
    });
    (<FormArray>this.recipeIngredientsForm.get('ingredients')).push(ing)
  }

  onDeleteIngredient(index) {
    (this.recipeIngredientsForm.get('ingredients') as FormArray).removeAt(index)
  }

  get stepsControl() {
    return (this.recipeStepsForm.get('steps') as FormArray).controls;
  }

  onAddStep() {
    const s = new FormGroup({
      step: new FormControl('', Validators.required)
    });
    (<FormArray>this.recipeStepsForm.get('steps')).push(s);
  }

  onDeleteStep(index) {
    (<FormArray>this.recipeStepsForm.get('steps')).removeAt(index);
  }

  onSaveRecipe() {
    let recipe = new Recipe();
    recipe.title = this.recipeDetailForm.controls.title.value;
    recipe.description = this.recipeDetailForm.controls.description.value;
    recipe.imageUrl = this.recipeDetailForm.controls.imageUrl.value;
    recipe.cookTime = this.recipeDetailForm.controls.cookTime.value;
    recipe.readyTime = this.recipeDetailForm.controls.readyTime.value;
    recipe.serving = this.recipeDetailForm.controls.serving.value;
    recipe.cuisine = this.recipeDetailForm.controls.cuisine.value;
    recipe.ingredients = this.recipeIngredientsForm.controls.ingredients.value;
    recipe.steps = this.recipeStepsForm.controls.steps.value;
    // console.log(recipe)

    if (this.editMode) {
      this.recipeService.updateRecipe(this.id, recipe).subscribe(
        resp => {
          console.log('update success ', resp)
        },
        error => {
          console.log(error)
        }
      )
    } else {
      this.recipeService.saveRecipe(recipe).subscribe(
        resp => {
          console.log('save success ', resp)
        },
        error => {
          console.log(error)
        }
      )
    }
  }

}
