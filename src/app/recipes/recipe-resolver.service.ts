import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Recipe } from '../shared/model/recipe.model';
import { RecipesService } from './recipes.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class RecipeResolver implements Resolve<Recipe> {

    constructor(
        private recipeService: RecipesService,
        private router: Router
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> {
        // console.log('recipe id ', route.params['id'])
       
        return this.recipeService.getRecipeById(route.params['id'])
            .pipe(catchError((err) => this.router.navigateByUrl('/')))
    }
}