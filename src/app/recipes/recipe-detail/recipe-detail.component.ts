import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Recipe } from '../../shared/model/recipe.model';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/auth/user.model';
import { RecipesService } from '../recipes.service';
import { concatMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  id: string;
  recipe: Recipe;
  user: User
  isAuthenticated = false;
  isLoading = false;

  constructor(
    private recipeSerivce: RecipesService,
    private route: ActivatedRoute,
    public router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.isLoading = true;

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
    });

    this.route.data.pipe(
      concatMap(data => {
        this.recipe = data.recipe
        this.isLoading = false;

        return this.authService.user.pipe(tap(
          (user: User) => {
            // console.log(user)
            this.isAuthenticated = !user ? false : true;
            this.user = user;
          }
        ))
      })
    ).subscribe()
    
  }

  onDeleteRecipe(recipeId) {
    // console.log('recipe to delete ', recipeId)
    this.recipeSerivce.deleteRecipe(recipeId).subscribe(
      resp => {
        console.log('delete success ', resp)
        this.router.navigate['/recipes']
      },
      error => {
        console.log(error)
      }
    )
  }
}
