import { Injectable } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { map } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { Recipe } from '../shared/model/recipe.model';
import { User } from '../auth/user.model';
import { RecipesConfig } from '../shared/model/recipes-config.model';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RecipesService {

  recipeConfigChanged = new Subject<RecipesConfig>();

  constructor(private apiService: ApiService) { }

  saveRecipe(recipe: Recipe) {
    return this.apiService
      .post('/recipes', recipe)
      .pipe(map(data => data))
  }

  getRecipes() {
    return this.apiService
      .get('/recipes')
      .pipe(map(data => data))
  }

  getRecipesByConfig(config: RecipesConfig) {
    const params = {}
    Object.keys(config.filters).forEach(key => {
      params[key] = config.filters[key]
    })
    // console.log(params)

    return this.apiService
      .get('/recipes', new HttpParams({fromObject: params})
    )
  }

  getRecipeById(recipeId) {
    // return this.apiService
    //   .getById('/recipes', recipeId)
    //   .pipe(map(data => data))

    return this.apiService.get('/recipes/' + recipeId)
      .pipe(map((data: {recipe: Recipe}) => data.recipe));
  }

  updateRecipe(recipeId: string, recipe: Recipe) {
    return this.apiService
      .patch('/recipes', recipe, recipeId)
      .pipe(map(data => data))
  }

  deleteRecipe(recipeId: string) {
    return this.apiService
      .delete('/recipes', recipeId)
      .pipe(map(data => data))
  }

  updateFavoriteCount(recipeId: string, isFavorited: boolean) {
    if(isFavorited) {
      return this.apiService.post(`/recipes/${recipeId}/favorite`)
      .pipe(map(data => data))

    } else {
      return this.apiService.deleteNonStandard('/recipes', recipeId, 'favorite')
      .pipe(map(data => data))
    }
  }
}
