import { Component, OnInit } from '@angular/core';
import { Recipe } from '../shared/model/recipe.model';
import { RecipesService } from './recipes.service';
import { RecipesConfig } from '../shared/model/recipes-config.model';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {

  recipes: Recipe[];
  recipesConfig: RecipesConfig = {
    type: 'all',
    filters: {}
  };

  constructor(private recipeService: RecipesService) { }

  ngOnInit() {
    this.recipeService.recipeConfigChanged.next(this.recipesConfig)
  }
}
