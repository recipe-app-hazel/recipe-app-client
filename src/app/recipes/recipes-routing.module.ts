import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { RecipesComponent } from './recipes.component';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { RecipesEditorComponent } from './recipes-editor/recipes-editor.component';
import { AuthGuard } from '../auth/auth.guard';
import { RecipeResolver } from './recipe-resolver.service';

const routes: Routes = [
    {
        path: '', 
        component: RecipesComponent
    },
    {
        path: 'recipes/new',
        component: RecipesEditorComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'recipes/:id',
        component: RecipeDetailComponent,
        resolve: {
            recipe: RecipeResolver
        },
    },
    {
        path: 'recipes/:id/edit',
        component: RecipesEditorComponent,
        canActivate: [AuthGuard],
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class RecipesRoutingModule {}