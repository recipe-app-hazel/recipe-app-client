import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipesComponent } from './recipes.component';
import { RecipesRoutingModule } from './recipes-routing.module';
import { SharedModule } from '../shared/shared.module';
import { RecipesEditorComponent } from './recipes-editor/recipes-editor.component';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';


@NgModule({
  declarations: [
    RecipesComponent, 
    RecipesEditorComponent,
    RecipeDetailComponent
  ],
  imports: [
    CommonModule,
    RecipesRoutingModule,
    SharedModule,
    AngularMaterialModule
  ]
})
export class RecipesModule { }
