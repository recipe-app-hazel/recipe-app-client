import { Component, OnInit, OnDestroy } from '@angular/core';
import { Recipe } from 'src/app/shared/model/recipe.model';
import { User } from 'src/app/auth/user.model';
import { RecipesConfig } from 'src/app/shared/model/recipes-config.model';
import { ActivatedRoute } from '@angular/router';
import { RecipesService } from 'src/app/recipes/recipes.service';


@Component({
  selector: 'app-profile-favorited-recipes',
  templateUrl: './profile-favorited-recipes.component.html',
  styleUrls: ['./profile-favorited-recipes.component.css']
})
export class ProfileFavoritedRecipesComponent implements OnInit {

  recipes: Recipe[]
  error = null;

  constructor(
    private route: ActivatedRoute,
    private recipeService: RecipesService
  ) { }

  user: User;
  recipesConfig: RecipesConfig = {
    type: 'all',
    filters: {}
  };

  ngOnInit() {
    // console.log('profile favorited recipes')
    this.route.parent.data.subscribe(
      (data: { user: User }) => {
        this.user = data.user;
        this.recipesConfig.filters.favorited = this.user.username;

        this.recipeService.recipeConfigChanged.next(this.recipesConfig)
      }
    );
  }
}
