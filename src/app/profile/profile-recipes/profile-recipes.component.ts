import { Component, OnInit, OnDestroy } from '@angular/core';
import { Recipe } from 'src/app/shared/model/recipe.model';
import { RecipesConfig } from 'src/app/shared/model/recipes-config.model';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/auth/user.model';
import { RecipesService } from 'src/app/recipes/recipes.service';

@Component({
  selector: 'app-profile-recipes',
  templateUrl: './profile-recipes.component.html',
  styleUrls: ['./profile-recipes.component.css']
})
export class ProfileRecipesComponent implements OnInit {

  recipes: Recipe[]
  user: User;

  recipesConfig: RecipesConfig = {
    type: 'all',
    filters: {}
  };

  constructor(
    private route: ActivatedRoute,
    private recipeService: RecipesService
  ) { }

  ngOnInit() {
    // console.log('profile all recipes')
    this.route.parent.data.subscribe(
      (data: { user: User }) => {
        // console.log(data.user)
        this.user = data.user;
        this.recipesConfig.filters.author = this.user.username;

        if(this.user.username !== data.user.username) {
          this.recipeService.recipeConfigChanged.next(this.recipesConfig)
        }
      }
    );
  }
}
