import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRecipesComponent } from './profile-recipes/profile-recipes.component';
import { ProfileFavoritedRecipesComponent } from './profile-favorited-recipes/profile-favorited-recipes.component';
import { ProfileComponent } from './profile/profile.component';

import { ProfileRoutingModule } from './profile-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AngularMaterialModule } from '../angular-material/angular-material.module';



@NgModule({
  declarations: [
    ProfileRecipesComponent,
    ProfileFavoritedRecipesComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AngularMaterialModule,
    ProfileRoutingModule
  ]
})
export class ProfileModule { }
