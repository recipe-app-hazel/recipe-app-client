import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { User } from '../auth/user.model';
import { ProfileService } from './profile.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ProfileResolver implements Resolve<User> {

    constructor(
        private profileService: ProfileService,
        private router: Router
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> {
        // console.log('username ', route.params['username'])
       
        return this.profileService.get(route.params['username'])
            .pipe(catchError((err) => this.router.navigateByUrl('/')))
    }
}