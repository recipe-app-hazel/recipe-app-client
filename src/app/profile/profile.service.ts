import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../auth/user.model';
import { ApiService } from '../shared/services/api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private apiService: ApiService) { }

  get(username: string): Observable<User> {
    return this.apiService.get('/users/' + username)
      .pipe(map((data: {user: User}) => data.user));
  }
}
