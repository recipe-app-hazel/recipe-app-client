import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileRecipesComponent } from './profile-recipes/profile-recipes.component';
import { ProfileFavoritedRecipesComponent } from './profile-favorited-recipes/profile-favorited-recipes.component';
import { AuthGuard } from '../auth/auth.guard';
import { ProfileComponent } from './profile/profile.component';
import { ProfileResolver } from './profile-resolver.service';

const routes: Routes = [
    {
        path: ':username',
        component: ProfileComponent,
        resolve: {
            user: ProfileResolver
        },
        children: [
            {
                path: '',
                component: ProfileRecipesComponent
            },
            {
                path: 'favorites',
                component: ProfileFavoritedRecipesComponent
            },
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ProfileRoutingModule { }