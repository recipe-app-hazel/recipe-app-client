import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { concatMap, tap } from 'rxjs/operators';
import { User } from 'src/app/auth/user.model';
import { AuthService } from 'src/app/auth/auth.service';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  isLoginUser: boolean
  profile: User
  navLinks = []


  constructor(
    private authService: AuthService,
    private profileService: ProfileService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.route.data.pipe(
      concatMap(data => {
        // console.log('resolving...')
        this.profile = data.user;

        this.navLinks = [
          { label: 'My recipes', path: `/profile/${this.profile.username}` },
          { label: 'Favorited recipes', path: `/profile/${this.profile.username}/favorites` }
        ]

        return this.authService.user.pipe(tap(
          (userData: User) => {
            if (userData) {
              this.isLoginUser = (userData.username === this.profile.username);
            }
          }
        ));
      })
    ).subscribe();


    this.route.params.subscribe(params => {
      const author = params['username']

      this.navLinks = [
        { label: 'My recipes', path: `/profile/${author}` },
        { label: 'Favorited recipes', path: `/profile/${author}/favorites` }
      ]

      this.profileService.get(author).subscribe(
        data => {
          this.profile = data
          this.authService.user.subscribe((userData: User) => {
            if(userData) {
              this.isLoginUser = (userData.username === this.profile.username);
            }
          })
        }
      )
    });
  }


  onFollowUser() {
    alert('Function not implement ...')
  }

}
