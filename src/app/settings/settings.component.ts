import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { User } from '../auth/user.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  currentUserForm: FormGroup

  constructor(
    private authService: AuthService
  ) {}

  ngOnInit() {
    let username = ''
    let email = ''
    let phone = ''
    let gender = ''
    let city = ''
    let country = ''

    this.authService.user.subscribe(
      data => {
        // console.log('current user ', data)
       
        username = data.username
        email = data.email

        this.currentUserForm = new FormGroup({
          username: new FormControl(username, Validators.required),
          email: new FormControl(email, [Validators.email, Validators.required]),
          phone: new FormControl(phone, Validators.required),
          city: new FormControl(city, Validators.required),
          country: new FormControl(country, Validators.required)
        })

      },
      error => {
        console.log(error)
      }
    )
  }


  updateProfile() {
    alert('to implement ...')
  }
}
