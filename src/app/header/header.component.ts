import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { AuthService } from '../auth/auth.service';
import { RecipesService } from '../recipes/recipes.service';
import { AuthComponent } from '../auth/auth.component';
import { User } from '../auth/user.model';
import { RecipesConfig } from '../shared/model/recipes-config.model';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  collapsed = true;
  isAuthenticated = false;
  userSubs: Subscription;
  currentUser: User;

  dialogRef: MatDialogRef<any>;

  recipesConfig: RecipesConfig = {
    type: 'all',
    filters: {}
  };

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private authService: AuthService,
    private recipeService: RecipesService
  ) { }

  ngOnInit() {
    this.userSubs = this.authService.user.subscribe(user => {
      // console.log(user)
      if(user) this.currentUser = user;
      this.isAuthenticated = !user ? false : true;
    })
  }

  ngOnDestroy() {
    this.userSubs.unsubscribe()
  }

  openDialog(): void {
    if(!this.dialogRef) {
      this.dialogRef = this.dialog.open(AuthComponent, {
        width: '50%'
      });

    } else {
      this.dialogRef.close()
      this.dialogRef = this.dialog.open(AuthComponent, {
        width: '50%'
      });
    }

    this.dialogRef.afterClosed().subscribe(result => {
      // console.log('dialog closed')
    });
  }

  onLogout() {
    this.authService.logout()
  }

  onUsernameClicked() {
    this.router.navigateByUrl(`/profile/${this.currentUser.username}`)
    
    this.recipesConfig.filters.author = this.currentUser.username
    this.recipeService.recipeConfigChanged.next(this.recipesConfig)
  }
}
