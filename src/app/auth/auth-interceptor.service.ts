import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';
import { take, exhaustMap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

    constructor(private authService: AuthService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        // console.log('intercept ...')
        return this.authService.user.pipe(
            take(1),
            exhaustMap(user => {
                const headersConfig = {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                };

                if (user && user.token) {
                    headersConfig['Authorization'] = `Token ${user.token}`;
                    const modifiedReq = req.clone({ setHeaders: headersConfig });
                    return next.handle(modifiedReq)
                }

                return next.handle(req);
            })
        )
    }
}