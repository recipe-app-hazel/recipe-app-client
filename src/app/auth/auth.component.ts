import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  loading: boolean = false;
  error = null;

  authMode: string;
  title: string;

  authForm: FormGroup;
  username: FormControl;
  email: FormControl;
  password: FormControl;



  constructor(
    private route: Router,
    private authService: AuthService,
    private dialogRef: MatDialogRef<AuthComponent>
  ) {
    this.username = new FormControl(null, Validators.required);
    this.email = new FormControl(null, [Validators.required, Validators.email]);
    this.password = new FormControl(null, Validators.required);

    this.authForm = new FormGroup({});
  }


  ngOnInit() {
    this.authMode = 'login';
    this.title = 'Login';

    this.onSwitchPage(this.authMode)
  }

  onSwitchPage(page) {
    Object.keys(this.authForm.controls).forEach(control => {
      this.authForm.removeControl(control);
    });

    switch (page) {
      case 'login':
        this.authMode = 'login';
        this.title = 'Login'
        this.authForm.addControl('email', this.email)
        this.authForm.addControl('password', this.password)
        break;

      case 'register':
        this.authMode = 'register';
        this.title = 'Register'
        this.authForm.addControl('username', this.username)
        this.authForm.addControl('email', this.email)
        this.authForm.addControl('password', this.password)
        break;

      case 'forgotPassword':
        this.authMode = 'forgotPassword';
        this.title = 'Reset password'
        this.authForm.addControl('email', this.email)
        break;
    }
  }

  onAction(authMode) {

    switch (authMode) {
      case 'login':
        this.authService.login(
          this.authForm.controls.email.value,
          this.authForm.controls.password.value
        ).subscribe(
          resp => {
            this.dialogRef.close();
          },
          err => {
            console.log('login error ', err.error)
            this.showError(err.error)
          }
        )
        break;

      case 'register':
        this.authService.register(
          this.authForm.controls.username.value,
          this.authForm.controls.email.value,
          this.authForm.controls.password.value
        ).subscribe(
          resp => {
            this.dialogRef.close();
            // TODO: 
            // auth service to send email verification
            this.route.navigateByUrl('/')
          },
          err => {
            console.log('register error', err.error)
            this.showError(err.error)
          }
        )
        break;
    }
  }


  private showError(error: string) {
    this.loading = false;
    this.error = error;
    setTimeout(() => this.error = null, 3000);
  }
}
