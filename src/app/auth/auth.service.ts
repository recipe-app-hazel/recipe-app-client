import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { User } from './user.model';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from '../shared/services/api.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user = new BehaviorSubject<User>(null);
  private tokenExpirationTimer: any;

  constructor(
    private apiService: ApiService,
    private router: Router
  ) { }

  autoLogin() {
    const userData = JSON.parse(localStorage.getItem('recipe-user'))
    if (!userData) {
      return;
    }

    const loadedUser = new User(
      userData.email,
      userData.userId,
      userData.username,
      userData._token,
      new Date(userData._tokenExpirationDate)
    )

    // console.log('loaded user ', loadedUser)
    if (loadedUser.token) {
      this.user.next(loadedUser)
      const expiryDuration =
        new Date(loadedUser._tokenExpirationDate).getTime() -
        new Date().getTime()

      this.autoLogout(expiryDuration)
    }
  }

  autoLogout(expiryDuration: number) {
    this.tokenExpirationTimer = setTimeout(() => {
      this.logout();
    }, expiryDuration);
  }

 
  register(username: string, email: string, password: string) {
    return this.apiService
      .post('/users/register', {username, email, password})
      .pipe(map(data => {
        console.log('[authService] - register ', data)
        this.authSuccess(data)
      }))
  }


  login(email: string, password: string) {
    return this.apiService
      .post('/users/login', { email, password })
      .pipe(map(data => {
        console.log('[authService] - login ', data)
        this.authSuccess(data)
      }))
  }

  logout() {
    this.user.next(null)
    this.router.navigateByUrl('/')
    localStorage.removeItem('recipe-user')

    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer)
    }

    this.tokenExpirationTimer = null;
  }

  private authSuccess(data) {
    const decoded = jwt_decode(data.token)

    this.handleAuthentication(
      data.user._id,
      data.user.email,
      data.user.username,
      data.token,
      +decoded.exp
    )
  }

  private handleAuthentication(
    userId: string,
    email: string,
    username: string,
    token: string,
    expiredDate: number) {

    const expirationDate = new Date(expiredDate * 1000);

    // console.log('expiration date: ', expirationDate)
    const user = new User(email, userId, username, token, expirationDate);
    this.user.next(user);

    let expiresIn: number =
      (expiredDate * 1000) -
      (new Date().getTime())

    // console.log('expires in ', expiresIn)
    this.autoLogout(expiresIn);

    // console.log('user stored: ', user);
    localStorage.setItem('recipe-user', JSON.stringify(user));
  }

  getCurrentUser(): User{
    return this.user.value
  }
}
