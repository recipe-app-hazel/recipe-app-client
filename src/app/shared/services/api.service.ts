import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';


@Injectable({ providedIn: 'root' })
export class ApiService {
    constructor(
        private http: HttpClient
    ) { }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };

    private formatErrors(error: any) {
        return throwError(error.error);
    }

    get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
        return this.http
            .get(`${environment.api_url}${path}`, { params })
            .pipe(
                retry(3),
                catchError(this.formatErrors));
    }

    getById(path: string, id: string): Observable<any> {
        return this.http
            .get(`${environment.api_url}${path}/${id}`)
            .pipe(
                retry(3),
                catchError(this.formatErrors));
    }

    post(path: string, body: Object = {}): Observable<any> {
        return this.http
            .post(`${environment.api_url}${path}`, body)
            .pipe(
                retry(3),
                catchError(this.formatErrors));
    }

    patch(path: string, body: Object = {}, id: string): Observable<any> {
        return this.http
            .patch(`${environment.api_url}${path}/${id}`, body)
            .pipe(
                retry(3),
                catchError(this.formatErrors));
    }

    delete(path: string, id: string): Observable<any> {
        return this.http
            .delete(`${environment.api_url}${path}/${id}`)
            .pipe(
                retry(3),
                catchError(this.formatErrors));
    }

    deleteNonStandard(path: string, id: string, key: string): Observable<any> {
        return this.http
            .delete(`${environment.api_url}${path}/${id}/${key}`)
            .pipe(
                retry(3),
                catchError(this.formatErrors));
    }
}
