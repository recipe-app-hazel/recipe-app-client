import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { User } from 'src/app/auth/user.model';
import { Recipe } from 'src/app/shared/model/recipe.model';
import { AuthService } from 'src/app/auth/auth.service';
import { RecipesService } from 'src/app/recipes/recipes.service';
import { AuthComponent } from 'src/app/auth/auth.component';


@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit, OnDestroy {

  @Input('recipe') recipe: Recipe

  userSubs: Subscription
  isAuthenticated = false;
  user: User

  isProfile = false;
  isFavorited = false;
  error = null;

  dialogRef: MatDialogRef<any>

  constructor(
    private router: Router,
    private authService: AuthService,
    private recipeService: RecipesService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.isProfile = this.router.url.includes('profile') ? true : false; 
  
    this.userSubs = this.authService.user.subscribe(user => {
      this.isAuthenticated = !user ? false : true;

      if(user) {
        const isLiked = this.recipe.favorite.favoritedBy.find(favUser => favUser == user.userId)
        this.isFavorited = isLiked ? true : false;
      }
    })
  }

  onFavoriteClicked() {
    // console.log('isAuth ', this.isAuthenticated)
    if (!this.isAuthenticated) {
      this.dialogRef = this.dialog.open(AuthComponent, {
        width: '50%'
      });

      this.dialogRef.afterClosed().subscribe(result => {
        // console.log('The dialog was closed');
      });

    } else {
      this.isFavorited = !this.isFavorited;

      this.recipeService.updateFavoriteCount(this.recipe._id, this.isFavorited).subscribe(
        resp => {
          if (!resp.error) {
            // console.log(resp.recipe)
            this.recipe = resp.recipe
          } else {
            this.error = resp.error
          }
        },
        error => {
          this.error = error.error
        }
      )
    }

  }

  ngOnDestroy() {
    this.userSubs.unsubscribe
  }
}
