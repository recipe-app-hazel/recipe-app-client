import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { RecipesService } from 'src/app/recipes/recipes.service';
import { Recipe } from '../model/recipe.model';
import { RecipesConfig } from '../model/recipes-config.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {

  configSubs: Subscription;

  query: RecipesConfig
  recipes: Recipe[] = []
  loading = false
  currentPage = 1
  recipesCount: number

  @Input('limit') limit: number
  @Input('config')
  set config(config: RecipesConfig) {
    if (config) {
      this.query = config
      this.currentPage = 1
    }
  }

  constructor(private recipesService: RecipesService) { }

  ngOnInit() {
    // console.log('querying recipes ', this.query)
    this.loading = true;

    this.configSubs = this.recipesService.recipeConfigChanged.subscribe(config => {
      // console.log('recipe config changed')
      this.getRecipes(config)
    })

    this.getRecipes(this.query)
  }

  getRecipes(config) {
    this.recipesService.getRecipesByConfig(config).subscribe(
      data => {
        // console.log(data)
        this.loading = false;
        this.recipes = data.recipes;
        this.recipesCount = data.count;

        // this.totalPages = Array.from(new Array(Math.ceil(data.count / this.limit)), (val, index) => index + 1);
      },
      error => {
        console.log(error)
      }
    )
  }

  ngOnDestroy() {
    this.configSubs.unsubscribe()
  }
}
