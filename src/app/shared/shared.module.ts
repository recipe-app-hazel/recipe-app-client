import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/angular-material.module';

import { ShortenPipe } from './pipes/shorten.pipe';
import { RecipeItemComponent } from './recipe-item/recipe-item.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { NavLinkComponent } from './nav-link/nav-link.component';
import { LoaderComponent } from './loader/loader.component';


@NgModule({
  declarations: [
    ShortenPipe,
    RecipeItemComponent,
    RecipeListComponent,
    NavLinkComponent,
    LoaderComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule,
    AngularMaterialModule,
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    ShortenPipe,
    RecipeItemComponent,
    RecipeListComponent,
    NavLinkComponent,
    LoaderComponent
  ]
})
export class SharedModule { }
