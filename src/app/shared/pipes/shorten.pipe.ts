import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'shorten'
})
export class ShortenPipe implements PipeTransform {
    transform(paragraph: any, limit: number) {

        // if (paragraph.length > limit) {
        //     return paragraph.substr(0, limit);
        // }

        if (paragraph.split(' ').length > limit) {
            let wordsArr = paragraph.split(' ')
            return wordsArr.slice(0, limit).join(' ')
        }

        return paragraph;
    }
}