import { Ingredient } from './ingredient.model';
import { Step } from './steps.model';

export class Recipe {
    _id: string;
    title: string;
    description: string;
    imageUrl: string;
    serving: string;
    cookTime: string;
    readyTime: string;
    cuisine: string;
    ingredients: Ingredient[];
    steps: Step[];
    author: any;
    favorite: any;
}