export interface RecipesConfig {
    type: string;

    filters: {
        author?: string,
        favorited?: string,
        limit?: number
    }
}